using System;
using System.Reflection;
using System.IO;
using System.Data;
using System.Threading;
using Sylvia.Common;

namespace Presentation.Web.AspDotNetCore.Client
{
    /// <summary>
    /// Summary description for Broker.
    /// </summary>
    public class BrokerUI
    {
        public BrokerUI()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        static private DataSet Execute(string sAssemly, string sClass, DataSet ds)
        {
            try
            {
                // load the dll
                Assembly loadedAssembly = Assembly.LoadFrom(sAssemly);

                // load a class type
                Type loadedClass = loadedAssembly.GetType(sClass);

                // create an instance of the type
                Object myClassObj = Activator.CreateInstance(loadedClass);

                // define the parameter data type of the method
                Type[] arrType = new Type[1];
                arrType.SetValue(typeof(System.Data.DataSet), 0);

                // get method info
                MethodInfo mi = loadedClass.GetMethod("Execute", arrType);

                // invoke the method
                Object[] arrObj = { ds };

                object obj = mi.Invoke(myClassObj, arrObj);

                return (DataSet)obj;
            }
            catch (Exception ex)
            {
                ErrorDS errDS = new ErrorDS();
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DYNAMIC_INVOCATION_FAILED.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_UI.ToString();
                err.ErrorInfo1 = sAssemly;
                err.ErrorInfo2 = sClass;
                err.ActualMsg = ex.Message;
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                AppLogger.LogFatal(errDS.GetXml());
                return errDS;

            }

        }

        static public DataSet Request(DataSet ds)
        {
            #region Update By Jarif, 29, Feb 2012...
            #region Old...
            //if (Thread.CurrentPrincipal.Identity.Name.ToLower().Trim() != "admin")
            //{
            //    ActionDS actionDS = new ActionDS();
            //    actionDS.Merge(ds.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error);
            //    actionDS.AcceptChanges();
            //    string sAction = actionDS.Actions[0].ActionID.Trim();
            //    ActionID actionID = ActionID.ACTION_UNKOWN_ID;
            //    actionID = (ActionID)Enum.Parse(typeof(ActionID), sAction, true);
            //    int nActionId = (int)actionID;
            //    #region jarif(14Feb11)
            //    //if ((ActionID.ACTION_USER_AUTHENTICATE.ToString() != sAction && ActionID.ACTION_SEC_USER_LOGOUT.ToString() != sAction) && Thread.CurrentPrincipal.IsInRole(nActionId.ToString()) == false)
            //    //{
            //    //    HttpContext.Current.Session[DictionaryUI.SessionKey.KEY_MSG.ToString()] = DictionaryUI.ERR_MSG_INVALID_PRIVILEGE;
            //    //    HttpContext.Current.Response.Redirect("frmError.aspx");
            //    //}
            //    string[] strArrAction=  sAction.Split('_');
            //    if (strArrAction[0] != "NA")
            //    {
            //        if ((ActionID.ACTION_USER_AUTHENTICATE.ToString() != sAction && ActionID.NA_ACTION_SEC_USER_LOGOUT.ToString() != sAction) && Thread.CurrentPrincipal.IsInRole(nActionId.ToString()) == false)
            //        {
            //            HttpContext.Current.Session[DictionaryUI.SessionKey.KEY_MSG.ToString()] = DictionaryUI.ERR_MSG_INVALID_PRIVILEGE;
            //            HttpContext.Current.Response.Redirect("frmError.aspx");
            //        }
            //    }
            //    #endregion
            //}
            #endregion

            // WALI :: 11-Aug-2016 :: To check user's login state
            string sAction = "";
            ActionDS actionDS = new ActionDS();
            actionDS.Merge(ds.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error);
            actionDS.AcceptChanges();
            sAction = actionDS.Actions[0].ActionID.Trim();
            // WALI :: End :: 11-Aug-2016

            string userRoleName = "";
            //if (HttpContext.Current.Session["UserRoleName"] != null) userRoleName = Convert.ToString(HttpContext.Current.Session["UserRoleName"]);

            //if (Thread.CurrentPrincipal.Identity.Name.ToLower().Trim() != "admin" && userRoleName != "admin" && userRoleName != "admin_branch" && userRoleName != "user" && userRoleName != "admin_zone" && userRoleName != "admin_region" && userRoleName != "admin_division")
            //{
            //    //ActionDS actionDS = new ActionDS();
            //    //actionDS.Merge(ds.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error);
            //    //actionDS.AcceptChanges();

            //    //string sAction = actionDS.Actions[0].ActionID.Trim();   // WALI :: 11-Aug-2016 :: Done ABOVE
            //    ActionID actionID = ActionID.ACTION_UNKOWN_ID;
            //    actionID = (ActionID)Enum.Parse(typeof(ActionID), sAction, true);

            //    int nActionId = (int)actionID;
            //    string[] strArrAction = sAction.Split('_');

            //    if (strArrAction[0] != "NA")
            //    {
            //        if ((ActionID.ACTION_USER_AUTHENTICATE.ToString() != sAction && ActionID.NA_ACTION_SEC_USER_LOGOUT.ToString() != sAction) && Thread.CurrentPrincipal.IsInRole(nActionId.ToString()) == false)
            //        {
            //            #region Jarif, 22.Jun.2015...
            //            UserDS.UAARoleDataTable UAARole = new UserDS.UAARoleDataTable();

            //            if (HttpContext.Current.Session["UARoleTbl"] != null)
            //            {
            //                UAARole = (UserDS.UAARoleDataTable)HttpContext.Current.Session["UARoleTbl"];
            //            }
            //            DataRow[] foundRole = UAARole.Select("ActionID = '" + sAction + "'");
            //            #endregion

            //            if (foundRole.Length == 0)
            //            {
            //                HttpContext.Current.Session[DictionaryUI.SessionKey.KEY_MSG.ToString()] = DictionaryUI.ERR_MSG_INVALID_PRIVILEGE;
            //                HttpContext.Current.Response.Redirect("frmError.aspx");
            //            }
            //        }
            //    }
            //}
            
            
            #endregion
            ////////////////////
            string sDispatcherBLAssembly = "Sylvia.BLL.Dispatcher.dll";  //ConfigurationSettings.AppSettings["DispatcherBLAssembly"];
            sDispatcherBLAssembly = (sDispatcherBLAssembly == null ? String.Empty : sDispatcherBLAssembly.Trim());
           // string sBinDir = "G:\\rnd-dot-net\\NgSylviaV1\\Sylvia\\Sylvia.UI.Web.Payroll\\Bin_Global"; //ConfigurationSettings.AppSettings["BinDir"]; C:\\inetpub\\wwwroot\\sylvia-64-bit\\
            string sBinDir = @"D:\SYLVIA-RND-ASP-DOT-DOT-CORE\asp-dot-net-core-sylvia\Sylvia\Sylvia.UI.Web.Payroll\Bin_Global";
            //string sBinDir = "C:\\inetpub\\wwwroot\\sylvia-64-bit\\Bin_Global"; //ConfigurationSettings.AppSettings["BinDir"]; C:\\inetpub\\wwwroot\\sylvia-64-bit\\
            sBinDir = (sBinDir == null ? String.Empty : sBinDir.Trim());
            string sDispatcherBLClass = "Sylvia.BLL.Dispatcher.DispatcherBL"; //ConfigurationSettings.AppSettings["DispatcherBLClass"];
            sDispatcherBLClass = (sDispatcherBLClass == null ? String.Empty : sDispatcherBLClass.Trim());
            string sDllPath = String.Empty;

            if (Directory.Exists(sBinDir) == false)
            {
                ErrorDS errDS = getConfigEntryError();
                errDS.Errors[0].ErrorInfo1 = "BinDir";
                errDS.Errors[0].ErrorInfo2 = sBinDir;
                errDS.Errors[0].ActualMsg = "Directory does not exist.";
                errDS.AcceptChanges();
                AppLogger.LogFatal(errDS.GetXml());
                return errDS;
            }

            try
            {
                sDllPath = Path.Combine(sBinDir, sDispatcherBLAssembly);
            }
            catch (Exception ex)
            {
                ErrorDS errDS = getConfigEntryError();
                errDS.Errors[0].ErrorInfo1 = "DispatcherBLAssembly";
                errDS.Errors[0].ErrorInfo2 = sDispatcherBLAssembly;
                errDS.Errors[0].ActualMsg = ex.Message;
                errDS.AcceptChanges();
                AppLogger.LogFatal(errDS.GetXml());
                return errDS;
            }

            if (File.Exists(sDllPath) == false)
            {
                ErrorDS errDS = getConfigEntryError();
                errDS.Errors[0].ErrorInfo1 = "DispatcherBLAssembly";
                errDS.Errors[0].ErrorInfo2 = sDispatcherBLAssembly;
                errDS.Errors[0].ActualMsg = "File does not exist.";
                errDS.AcceptChanges();
                AppLogger.LogFatal(errDS.GetXml());
                return errDS;
            }

            //#region WALI :: Check this user's Login State
            //if (sAction.Trim() != ActionID.NA_ACTION_SEC_USER_LOGOUT.ToString())  // Let pass when user is logging out.
            //{
            //    string loginID = Thread.CurrentPrincipal.Identity.Name.ToLower().Trim();
            //    //bool isUserLoggedIn = BrokerUI.Check_UserLoginState(loginID, sDllPath, sDispatcherBLClass);
            //    bool isUserLoggedIn = BrokerUI.Check_UserLoginState(loginID);
            //    if (!isUserLoggedIn)
            //    {
            //        HttpContext.Current.Session[DictionaryUI.SessionKey.KEY_MSG.ToString()] = DictionaryUI.ERR_MSG_SESSION_ENDED; ;
            //        HttpContext.Current.Response.Redirect("~/frmError.aspx");
            //    }
            //}
            //#endregion

            return BrokerUI.Execute(sDllPath, sDispatcherBLClass, ds);

        }

        # region Created by Asif, 11-mar-12
        //static public DataSet RequestForJobApplication(DataSet ds)
        //{
        //    string sDispatcherBLAssembly = ConfigurationSettings.AppSettings["DispatcherBLAssembly"];
        //    sDispatcherBLAssembly = (sDispatcherBLAssembly == null ? String.Empty : sDispatcherBLAssembly.Trim());
        //    string sBinDir = ConfigurationSettings.AppSettings["BinDir"];
        //    sBinDir = (sBinDir == null ? String.Empty : sBinDir.Trim());
        //    string sDispatcherBLClass = ConfigurationSettings.AppSettings["DispatcherBLClass"];
        //    sDispatcherBLClass = (sDispatcherBLClass == null ? String.Empty : sDispatcherBLClass.Trim());
        //    string sDllPath = String.Empty;

        //    if (Directory.Exists(sBinDir) == false)
        //    {
        //        ErrorDS errDS = getConfigEntryError();
        //        errDS.Errors[0].ErrorInfo1 = "BinDir";
        //        errDS.Errors[0].ErrorInfo2 = sBinDir;
        //        errDS.Errors[0].ActualMsg = "Directory does not exist.";
        //        errDS.AcceptChanges();
        //        AppLogger.LogFatal(errDS.GetXml());
        //        return errDS;
        //    }

        //    try
        //    {
        //        sDllPath = Path.Combine(sBinDir, sDispatcherBLAssembly);
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorDS errDS = getConfigEntryError();
        //        errDS.Errors[0].ErrorInfo1 = "DispatcherBLAssembly";
        //        errDS.Errors[0].ErrorInfo2 = sDispatcherBLAssembly;
        //        errDS.Errors[0].ActualMsg = ex.Message;
        //        errDS.AcceptChanges();
        //        AppLogger.LogFatal(errDS.GetXml());
        //        return errDS;

        //    }

        //    if (File.Exists(sDllPath) == false)
        //    {
        //        ErrorDS errDS = getConfigEntryError();
        //        errDS.Errors[0].ErrorInfo1 = "DispatcherBLAssembly";
        //        errDS.Errors[0].ErrorInfo2 = sDispatcherBLAssembly;
        //        errDS.Errors[0].ActualMsg = "File does not exist.";
        //        errDS.AcceptChanges();
        //        AppLogger.LogFatal(errDS.GetXml());
        //        return errDS;
        //    }
        //    return BrokerUI.Execute(sDllPath, sDispatcherBLClass, ds);

        //}
        # endregion

        static private ErrorDS getConfigEntryError()
        {
            ErrorDS errDS = new ErrorDS();
            ErrorDS.Error err = errDS.Errors.NewError();
            err.ErrorCode = ErrorCode.ERR_INVALID_CONFIG_ENTRY.ToString();
            err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            err.ErrorTier = ErrorTier.ERR_TIER_UI.ToString();
            errDS.Errors.AddError(err);
            errDS.AcceptChanges();
            return errDS;
        }

        #region WALI :: Check this user's Login State :: 10-Aug-2016
        #region OLD ....
        //static private bool Check_UserLoginState(string loginID, string sDllPath, string sDispatcherBLClass)
        //{
        //    if (loginID == "" || loginID == "admin") // EMPTY_STRING when user is not logged in
        //    {
        //        // When user is not LOGGED_IN or
        //        // When ADMIN user, let pass
        //        return true;
        //    }
        //    else
        //    {
        //        ErrorDS errDS = new ErrorDS();
        //        UserDS userDS = new UserDS();
        //        UserDS.User user = userDS.Users.NewUser();
        //        user.LoginId = loginID;
        //        userDS.Users.AddUser(user);

        //        ActionDS actionDS = new ActionDS();
        //        actionDS.Actions.AddAction(ActionID.NA_ACTION_USER_LOGIN_FLAG_CHECK.ToString());

        //        DataSet inputDS = new DataSet();
        //        inputDS.Merge(userDS);
        //        inputDS.Merge(actionDS);
        //        inputDS.AcceptChanges();

        //        DataSet responseDS = new DataSet();
        //        responseDS = BrokerUI.Execute(sDllPath, sDispatcherBLClass, inputDS);
        //        errDS.Merge(responseDS.Tables[errDS.Errors.TableName], false, MissingSchemaAction.Error);
        //        errDS.AcceptChanges();
        //        if (errDS.Errors.Count > 0) return false;

        //        DataBoolDS boolDS = new DataBoolDS();
        //        boolDS.Merge(responseDS.Tables[boolDS.DataBools.TableName], false, MissingSchemaAction.Error);
        //        boolDS.AcceptChanges();

        //        return boolDS.DataBools[0].BoolValue;
        //    }
        //}
        #endregion

        // Check From STATIC List
        static private bool Check_UserLoginState(string loginID)
        {
            if (loginID == "" || loginID == "admin") // EMPTY_STRING when user is not logged in
            {
                // When user is not LOGGED_IN or
                // When ADMIN user, let pass
                return true;
            }

            return LoginUserList.IsValidUser(loginID);
        }
        #endregion

        #region Rony :: 09 August 2016 :: For General Attendance
        //static public DataSet RequestForAttendance(DataSet ds)
        //{
        //    string sDispatcherBLAssembly = ConfigurationSettings.AppSettings["DispatcherBLAssembly"];
        //    sDispatcherBLAssembly = (sDispatcherBLAssembly == null ? String.Empty : sDispatcherBLAssembly.Trim());
        //    string sBinDir = ConfigurationSettings.AppSettings["BinDir"];
        //    sBinDir = (sBinDir == null ? String.Empty : sBinDir.Trim());
        //    string sDispatcherBLClass = ConfigurationSettings.AppSettings["DispatcherBLClass"];
        //    sDispatcherBLClass = (sDispatcherBLClass == null ? String.Empty : sDispatcherBLClass.Trim());
        //    string sDllPath = String.Empty;

        //    if (Directory.Exists(sBinDir) == false)
        //    {
        //        ErrorDS errDS = getConfigEntryError();
        //        errDS.Errors[0].ErrorInfo1 = "BinDir";
        //        errDS.Errors[0].ErrorInfo2 = sBinDir;
        //        errDS.Errors[0].ActualMsg = "Directory does not exist.";
        //        errDS.AcceptChanges();
        //        AppLogger.LogFatal(errDS.GetXml());
        //        return errDS;
        //    }

        //    try
        //    {
        //        sDllPath = Path.Combine(sBinDir, sDispatcherBLAssembly);
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorDS errDS = getConfigEntryError();
        //        errDS.Errors[0].ErrorInfo1 = "DispatcherBLAssembly";
        //        errDS.Errors[0].ErrorInfo2 = sDispatcherBLAssembly;
        //        errDS.Errors[0].ActualMsg = ex.Message;
        //        errDS.AcceptChanges();
        //        AppLogger.LogFatal(errDS.GetXml());
        //        return errDS;

        //    }

        //    if (File.Exists(sDllPath) == false)
        //    {
        //        ErrorDS errDS = getConfigEntryError();
        //        errDS.Errors[0].ErrorInfo1 = "DispatcherBLAssembly";
        //        errDS.Errors[0].ErrorInfo2 = sDispatcherBLAssembly;
        //        errDS.Errors[0].ActualMsg = "File does not exist.";
        //        errDS.AcceptChanges();
        //        AppLogger.LogFatal(errDS.GetXml());
        //        return errDS;
        //    }
        //    return BrokerUI.Execute(sDllPath, sDispatcherBLClass, ds);

        //}
        #endregion

    }  // end of class
}  // end of namespace
