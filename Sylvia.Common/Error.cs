using System;

namespace Sylvia.Common
{
  /// <summary>
  /// Summary description for ErrorCode.
  /// </summary>
	
  public enum ErrorLevel
  {
    ERR_LEVEL_NORMAL,
    ERR_LEVEL_WARNING,
    ERR_LEVEL_SEVER
  }
  public enum ErrorTier
  {
    ERR_TIER_UI,
    ERR_TIER_BL,
    ERR_TIER_DL
  }
  public enum ErrorCode
  {
    // security errors
    ERR_ATHENTICATION_FAILD,
    ERR_INVALID_USER_ROLE,
    
    ERR_NO_USER_FOUND,
    ERR_LOGIN_MAX_CONCURRENCY_EXCEED,
    ERR_LOGIN_MAX_FAILED_ATTEMPT_EXCEED,
    ERR_LOGIN_USER_EXPIRED,
    ERR_LOGIN_BEFORE_EFFECT_DATE,
    ERR_LOCKED_USER,
    ERR_INVALIED_PASSWORD,
    
    // database errors
    ERR_DB_CONNECTION_FAILED,
    ERR_DB_CONNECTION_CLOSE_FAILED,
    ERR_DB_TRANSACTION_FAILED,
    ERR_DB_COMMIT_FAILED,
    ERR_DB_ROLLBACK_FAILED,
    ERR_DB_OPERATION_FAILED, // generinc

    ERR_DB_COMMAND_NOT_FOUND,
    // configuration errors
    ERR_INVALID_CONFIG_ENTRY,
    ERR_DYNAMIC_INVOCATION_FAILED,
    ERR_XML,
    // data errors
    ERR_TABLE_HAS_ERROR,
    ERR_TABLE_HAS_ZERO_ROW,

    // begin of developer error codes
    ERR_DATA_TYPE_MISMATCH,
    ERR_DATA_MISSING,
    ERR_STRING_TOO_LONG,
    ERR_STRING_TOO_SHORT,
    ERR_DUPLICATE_CODE,
    
    ERR_CODE_NOT_FOUND,
    
    ERR_INVALID_FORMAT,
    ERR_NUMBER_OUT_OF_RANGE,
    ERR_DATE_OUT_OF_RANGE,
    // end of developers error code

    // action errors
    ERR_ACTION_HANDLER_NOT_FOUND,
    ERR_ACTION_MAP_NOT_FOUND,
    ERR_ACTION_NOT_SUPPORTED,
    ERR_ACTION_ID_INVALID,

    //Kaysar, 17-Jan-2010
    ERR_EMAIL_OPERATION_FAILED

  }
}
